﻿using System;
using System.Collections.Generic;

namespace SimplexMVC
{
	public class ClassConstraint : ICloneable
	{
		List<ClassX> _leftMember;
		List<ClassX> _rightMember;
		string _operator;
		string _option;

		public ClassConstraint()
		{
			_leftMember = new List<ClassX>();
			_rightMember = new List<ClassX>();
		}

		public List<ClassX> LeftMember
		{
			get { return _leftMember; }
			set { _leftMember = value; }
		}

		public string Option
		{
			get { return _option; }
			set { _option = value; }
		}

		public string Operator
		{
			get { return _operator; }
			set { _operator = value; }
		}

		public List<ClassX> RightMember
		{
			get { return _rightMember; }
			set { _rightMember = value; }
		}

		public void Multiply(int num){
			if (num < 0){
				switch(_operator){
					case ">=":
						_operator = "<=";
						break;
					case "<=":
						_operator = ">=";
						break;
					default:
					break;
				}
			}
			foreach(var x in _leftMember){
				x.Multiply(num);
			}
			foreach(var x in _rightMember){
				x.Multiply(num);
			}
		}

		public override string ToString()
		{
			string s = "";
			foreach(ClassX x in _leftMember){
				s += x.ToString() + " ";
			}
			s += _operator + " ";
			foreach(ClassX x in _rightMember){
				s += x.ToString() + " ";
			}
			s += _option;
			return s;
		}

		public ClassConstraint Clone()
		{
			var constraint = new ClassConstraint();
			constraint.Operator = _operator;

			constraint.LeftMember = new List<ClassX>();
			_leftMember.ForEach((item) => {
				constraint.LeftMember.Add(item.Clone());
			});

			_rightMember.ForEach((item) => {
				constraint.RightMember.Add(item.Clone());
			});

			constraint.Option = _option;

			return constraint;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}
	}
}
