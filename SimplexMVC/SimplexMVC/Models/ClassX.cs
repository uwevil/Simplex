﻿using System;
namespace SimplexMVC
{
	public class ClassX : ICloneable
	{
		string _name;
		int _index;
		string _secondSymbol = "";
		int _coeff;
		Fraction _fraction;

		public ClassX(string name, int index, int coeff)
		{
			_name = name;
			_index = index;
			_coeff = coeff;
			_fraction = new Fraction(coeff);
		}

		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		public int Index
		{
			get { return _index; }
			set { _index = value; }
		}

		public string SecondSymbol
		{
			get { return _secondSymbol; }
			set { _secondSymbol = value; }
		}

		public Fraction Fraction
		{
			get { return _fraction; }
			set { _fraction = value;}
		}


		public int Coeff
		{
			get { return _coeff; }
			set { _coeff = value; 
				_fraction = new Fraction(value);}
		}

		public void Multiply(int num){
			_coeff *= num;
			_fraction *= num;
		}

		public bool Compare(ClassX other){
			if (_name != other.Name)
				return false;
			if (_index != other.Index)
				return false;
			if (_coeff != other.Coeff)
				return false;
			if (_secondSymbol != other.SecondSymbol)
				return false;
			if (_fraction.Denominator != other.Fraction.Denominator 
			    || _fraction.Numerator != other.Fraction.Numerator)
				return false;
			return true;
		}

		public override string ToString(){
			return _coeff+_name+_index+_secondSymbol;
		}

		public ClassX Clone()
		{
			var x = new ClassX(_name, _index, _coeff);
			x.SecondSymbol = _secondSymbol;
			x.Fraction = _fraction;

			return x;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}
	}
}
