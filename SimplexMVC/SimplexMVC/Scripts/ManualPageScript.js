﻿
function del_row(no)
{
	var table=document.getElementById("data_table");
	var nbCons=(table.rows.length)-1;

	if (nbCons > 1){
		document.getElementById("nbCons").value = nbCons-1;
		document.getElementById("row"+no+"").outerHTML="";

		refresh_graphic();
	}
}

function add_row()
{
	var nbVars = document.getElementById("nbVars").value;

	var table=document.getElementById("data_table");
	var nbCons=(table.rows.length)-1;

	var currentRow = parseInt(document.getElementById("currentRow").value);

	var result = "<tr id='row"+currentRow+"'>"
		+"<th><input type='button' "
		+"name='btnDel"+currentRow+"' class='btnDel' "
		+"onclick='del_row("+currentRow+")'/></th>\n";
	var i = currentRow;
	var j;

	for (j=1; j<=nbVars; j++) {
		var cx=document.getElementById("new_cx"+j).value;
		result += "<th><input type='number' " +
			"id='c"+i+"x"+j+"' " +
			"name='c"+i+"x"+j+"' " +
			"value='"+cx+"' onchange='refresh_graphic()' required/>x<sub>"+j;
		result += "</sub></th>\n";
		if (j < nbVars)
			result += "<th>+</th>\n";
	}

	result += "<th>";
	result += "<select id='o"+currentRow+"' name='o" + currentRow + "' onchange='refresh_graphic()'>\n";
	var mySelect = document.getElementById("new_o");
	var valueSelected = mySelect.selectedIndex;
	if (valueSelected == 1)
		result += "<option value='<='>&le;</option>"
				+ "<option selected='selected' value='>='>&ge;</option>"
				+ "<option value='='>=</option>"
				+ "</select>\n";
	else{
		if (valueSelected == 0)
			result += "<option selected='selected' value='<='>&le;</option>"
				+ "<option value='>='>&ge;</option>"
				+ "<option value='='>=</option>"
				+ "</select>\n";
		else
			result += "<option value='<='>&le;</option>"
				+ "<option value='>='>&ge;</option>"
				+ "<option value='=' selected='selected'>=</option>"
				+ "</select>\n";
	}
	result += "</th>";
	result += "<th>\n";

	var c0 = document.getElementById("new_cx0").value;
	result += "<input type='number' id='c"+currentRow+"x0' name='c"+currentRow+"x0' value='"+c0
		+"'  onchange='refresh_graphic()' required/>\n";
	result += "</th>\n";

	if (nbVars == 2){
		result += "<th>";
		var color = document.getElementById("new_color").value;
		result += "<input type='color' id='color"+currentRow
					+"' name='color"+currentRow+"' value='"+color+"' onchange='refresh_graphic()'/>";
		result += "</th>";
	}

	result += "</tr>\n";

	var row = table.insertRow(nbCons).outerHTML=result;

	document.getElementById("currentRow").value = currentRow+1;
	document.getElementById("nbCons").value = nbCons+1;

	for (j=1; j<=nbVars; j++) {
		document.getElementById("new_cx"+j).value="1";
	}
	 document.getElementById("new_o").selectedIndex="0";
	 document.getElementById("new_cx0").value="1";
	 document.getElementById("new_color").value = "#ff0000";

	 refresh_graphic();
}

function refresh_graphic(){
	var nbVars = document.getElementById("nbVars").value;
	if (nbVars != 2){
		return;
	}

	var table=document.getElementById("data_table");
	var nbCons=(table.rows.length)-1;

	var currentRowIndex = 0;

	var coeffArray=[];
	var o=[];
	var colors=[];

	for (var i=0; i<nbCons; i++){
		coeffArray[i]=[];
		while(1){
			if (document.getElementById("c"+currentRowIndex+"x0") != null){
				break;
			}
			currentRowIndex = currentRowIndex + 1;
		}
		coeffArray[i][0] = parseInt(document.getElementById("c"+currentRowIndex+"x0").value)*-1 || 0;
		for (var j=1; j<=nbVars; j++){
			coeffArray[i][j] = parseInt(document.getElementById("c"+currentRowIndex+"x"+j).value) || 0;
		}
		o[i] = document.getElementById("o"+currentRowIndex).value;
		colors[i] = document.getElementById("color"+currentRowIndex).value;
		currentRowIndex = currentRowIndex + 1;
	}//for

	var tmp=[];
	var withOrWithout = document.getElementById("cond0").value == "avec" ? true : false;
	if (withOrWithout){
		tmp[1] = 1;
		tmp[2] = 0;
		tmp[0] = 0; //parseInt(document.getElementById("cond0x0").value)*-1 || 0;
		var condOperator = document.getElementById("cond0o").value;
		var condColor = document.getElementById("cond0color").value;

		coeffArray.push(tmp);
		o.push(condOperator);
		colors.push(condColor);
	}

	tmp=[];
	withOrWithout = document.getElementById("cond1").value == "avec" ? true : false;
	if(withOrWithout){
		tmp[2] = 1;
		tmp[1] = 0;
		tmp[0] = 0; //parseInt(document.getElementById("cond1x0").value)*-1 || 0;
		var condOperator = document.getElementById("cond1o").value;
		var condColor = document.getElementById("cond1color").value;

		coeffArray.push(tmp);
		o.push(condOperator);
		colors.push(condColor);
	}

	var xF = parseInt(document.getElementById("x0").value);
	var yF = parseInt(document.getElementById("x1").value);
	var colorXY = document.getElementById("colorXY").value;

	var minX=-10, minY=-10, maxX=10, maxY=10;

	for (var i = 0; i< nbCons; i++){
		if (coeffArray[i][1] != 0 && (coeffArray[i][0]/coeffArray[i][1])*-1 > maxX){
			maxX = coeffArray[i][0]*-1; // /coeffArray[i,1];
		}
		if (coeffArray[i][2] != 0 && (coeffArray[i][0]/coeffArray[i][2])*-1 > maxY){
			maxY = coeffArray[i][0]*-1; // /coeffArray[i,2];
		}
		if (coeffArray[i][1] != 0 && (coeffArray[i][0]/coeffArray[i][1])*-1 < minX){
			minX = coeffArray[i][0]*-1; // /coeffArray[i,1];
		}
		if (coeffArray[i][2] != 0 && (coeffArray[i][0]/coeffArray[i][2])*-1 < minY){
			minY = coeffArray[i][0]*-1; // /coeffArray[i,2];
		}
	}

	if (Math.abs(maxX) < Math.abs(minX)){
		maxX = Math.abs(minX);
	}else{
		minX = Math.abs(maxX)*-1;
	}

	if (Math.abs(maxY) < Math.abs(minY)){
		maxY = Math.abs(minY);
	}else{
		minY = Math.abs(maxY)*-1;
	}

	var b = JXG.JSXGraph.initBoard('box', {boundingbox: [minX, maxY, maxX, minY], axis:true});

	var functionM = b.create('line', [0, xF, yF], 
		{strokeColor: JXG.rgb2css(colorXY), dash:6, fixed: false});

	var line=[], ineq=[], intersection=[];


/*	var interTmp = getIntersection(condX[1], 0, condX0[1], 0, condX[0], condX0[0]);
	if (interTmp != null){
		intersection.push(b.create('point', [interTmp[0], interTmp[1]], 
			{withLabel:false, size:2, fixed: true}));
	}
	*/

	for (var i=0; i<coeffArray.length; i++){
		line[i] = b.create('line', coeffArray[i], 
			{strokeColor: JXG.rgb2css(colors[i]), fixed: true});
		if (o[i] == ">="){
			ineq[i] = b.create('inequality', [line[i]], {inverse: true, 
				fillColor: JXG.rgb2css(colors[i])});
		}else{
			if (o[i] == "<="){
				ineq[i] = b.create('inequality', [line[i]],
    				{fillColor: JXG.rgb2css(colors[i])});
    		}
		}

		if (i < coeffArray.length-1){
			for (var j=i+1; j<coeffArray.length; j++){
				interTmp = getIntersection(coeffArray[i][1], coeffArray[i][2],  coeffArray[i][0]*-1,
					 coeffArray[j][1],  coeffArray[j][2],  coeffArray[j][0]*-1);
				if (interTmp != null){
					intersection.push(b.create('point', [interTmp[0], interTmp[1]], 
						{withLabel:false, size:2, fixed: true}));
				}
			}//for			
		}

	}//for

//	var o0 = b.create('point', [0, 0], {withLabel:false, size: 2, color: 'black', fixed: true});
//	intersection.push(o0);

	var validPoints=[];
	var goal = document.getElementById("goal").value;
	var minmax = [];

	for (var i=0; i<intersection.length; i++){
		var x=parseInt(intersection[i].X());
		var	y=parseInt(intersection[i].Y());

		if (x < 0 || y < 0)
			continue;

		var isOk = true;
		for (var j=0; j<coeffArray.length; j++){
			var sum = x*coeffArray[j][1] + y*coeffArray[j][2];
			sum = sum.toPrecision(2);
			if (o[j] == "<="){
				if (sum > coeffArray[j][0]*-1){
					isOk =false;
					break;
				}
			}else{
				if (o[j] == ">="){
					if (sum < coeffArray[j][0]*-1){
						isOk =false;
						break;
					}
				}else{
					if (sum != coeffArray[j][0]*-1){
						isOk =false;
						break;
					}
				} // >=
			}// <=

		}//for
		if (isOk){
			validPoints.push(intersection[i]);
			if (minmax[0]  == null){
				minmax[0] = (intersection[i].X());
				minmax[1] = (intersection[i].Y());
			}else{
				var previousVal = minmax[0]*xF + minmax[1]*yF;
				var currentVal = intersection[i].X()*xF + intersection[i].Y()*yF;

				if (goal == "max"){
					if (previousVal < currentVal){
						minmax[0] = intersection[i].X();
						minmax[1] = intersection[i].Y();
					}
				}else{
					if (previousVal > currentVal){
						minmax[0] = intersection[i].X();
						minmax[1] = intersection[i].Y();
					}
				}	
			}
		} 
	}//for

	if (validPoints.length == 0)
		return;

	minX=validPoints[0].X()
	maxX=validPoints[0].X();
	minY=validPoints[0].Y()
	maxY=validPoints[0].Y();

	 for (var i = 1; i < validPoints.length; i++) {
        if (validPoints[i].X() < minX) minX = validPoints[i].X();
        if (validPoints[i].X() > maxX) maxX = validPoints[i].X();
        if (validPoints[i].Y() < minY) minY = validPoints[i].Y();
        if (validPoints[i].Y() > maxY) maxY = validPoints[i].Y();
    }

     // choose a "central" point
    var center = {
        x: minX + (maxX - minX) / 2,
        y: minY + (maxY - minY) / 2
    };

    var validPointsAngle=[];

    // precalculate the angles of each point to avoid multiple calculations on sort
    for (var i = 0; i < validPoints.length; i++) {
        validPointsAngle[i] = 
        	Math.acos((validPoints[i].X() - center.x) / lineDistance(center.x, center.y, 
        		validPoints[i].X(), validPoints[i].Y()));

        if (validPoints[i].Y() > center.y) {
            validPointsAngle[i] = Math.PI + Math.PI - validPointsAngle[i];
        }
    }

    var validPointsAngleTmp = clone(validPointsAngle);

     // sort by angle
    validPointAngle = validPointsAngle.sort(function(a, b) {
        return a - b;
    });

    var validPointsTmp=[];
    var perpendicular=[];

    var minmaxVal = minmax[0]*xF + minmax[1]*yF;
    var minmaxPoints=[];

    for (var i=0; i<validPointsAngle.length;i++){
    	for (var j=0; j<validPointsAngleTmp.length; j++){
    		if (validPointsAngle[i] == validPointsAngleTmp[j]){
    			validPoints[j].setAttribute({withLabel:false, size:2, fixed: true, color:'blue'});
    			validPointsTmp.push(validPoints[j]);

    			perpendicular.push(b.create('perpendicularsegment', [functionM, validPoints[j]], 
    				{withLabel:false, dash:1, color:JXG.rgb2css('#00FBF2')}));

    		/*	perpendicular[perpendicular.length-1].on('over', 
    				function(){
    					var text = this.point1.Dist(this.point2).toFixed(3);
    					this.setLabel(text);
    				});
    			perpendicular[perpendicular.length-1].on('out', 
    				function(){
    					this.setLabel('');
    				    this.setAttribute({withLabel: false});
    				});
    		*/
    			var currentVal = validPoints[j].X()*xF + validPoints[j].Y()*yF;
    			if (currentVal == minmaxVal){
    				minmaxPoints.push(validPoints[j]);
    			}

				break;
    		}
    	}
    }

  /*  var test = "";
    for (var i = 0; i<validPointsTmp.length; i++){
    	test += validPointsTmp[i].X() + ";" +validPointsTmp[i].Y() + "///";
    }
    alert(test);
    */
	var polygon = b.create('polygon', validPointsTmp, 
		{withLines: true, fillOpacity:1,fillColor: JXG.rgb2css(colorXY), 
			borders: {strokeWidth:1, strokeColor: JXG.rgb2css(colorXY)}});

	if (minmaxPoints.length > 0){
		for (var i=0; i<minmaxPoints.length; i++){
			minmaxPoints[i].setAttribute({withLabel: false, size:6, fixed: true, 
				face:'cross', color: JXG.rgb2css(colorXY)});
		}
	}

	for (var i=0; i< polygon.borders.length; i++){
		b.create('intersection', [functionM, polygon.borders[i]],
			{color:JXG.rgb2css(colorXY),visible:false});
	}

	b.highlightInfobox = function(x,y,el) {
            this.infobox.setText('<span style="color:black;font-weight:bold;'+
            		' font-size:15px">(' 
            	+ x + '; ' + y + ') </span>');
            this.infobox.rendNode.style.border = 'groove ' + el.visProp['strokeColor'] + ' 2px';
            this.infobox.rendNode.style.padding = '5px';
            this.infobox.rendNode.style.backgroundColor = 'white';};
}

function getIntersection(x1, y1, z1, x2, y2, z2){
	var det = x1*y2 - x2*y1;
	if (det == 0)
		return null;
	var x = (y2*z1 - y1*z2)/det;
	var y = (x1*z2 - x2*z1)/det;
	return [x, y];
}

function lineDistance(x1, y1, x2, y2) {
    var xs = 0;
    var ys = 0;

    xs = x2 - x1;
    xs = xs * xs;

    ys = y2 - y1;
    ys = ys * ys;

    return Math.sqrt(xs + ys);
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

