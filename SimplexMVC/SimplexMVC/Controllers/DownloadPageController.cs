﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimplexMVC.Controllers
{
    public class DownloadPageController : Controller
    {
		string currentMd5;

        public ActionResult Index()
        {
			currentMd5 = Request.Form["checked"];
			//ViewBag.result = "Download successful";
            //return View ();

			var dir = Path.GetTempPath();
			var fileName = "download_"+currentMd5+".txt";
			var fullPath = dir + fileName;

			byte[] fileBytes = Function.GetFile(fullPath);

			return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "save.txt");
        }
    }
}
