﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimplexMVC.Controllers
{
    public class UploadPageController : Controller
    {
		private string currentMd5;
		private string goal;
		private int nbVars;
		private int nbCons;
		List<ClassConstraint> constraints;
		ClassX[] func;
		List<ClassConstraint> conditions;

		[HttpPost]
		public ActionResult Index()
		{
			currentMd5 = Request.Form["checked"];
			var fileUpload = (HttpPostedFileBase)Request.Files["fileUpload"];
			if (fileUpload.ContentLength > 0) {
				try{
					string tempPath = Path.GetTempPath();
					fileUpload.SaveAs(tempPath+"upload_"+currentMd5+".txt");
					//	ViewBag.fileName = tempPath+"upload_"+currentMd5+".txt";
					ViewBag.fileName = "Upload successful";
				}catch (Exception ex){
					//	ViewBag.error = ex.Message + " " + currentMd5;
					ViewBag.error = ex.Message + " T.T";
					return View("Error");
				}
			}//if

			ViewBag.currentMd5 = currentMd5;

			string fileName = "upload_"+currentMd5 + ".txt";
			string fileDir = Path.GetTempPath();
			string fullPath = fileDir+fileName;

			try
			{   // Open the text file using a stream reader.
				using (StreamReader sr = new StreamReader(fullPath))
				{
					String line;

					while ((line = sr.ReadLine()) != null){
						string[] splitedString = line.Split(':');
						if (splitedString[0] == "goal"){
							goal = splitedString[1];
							continue;
						}
						if (splitedString[0] == "nbVars"){
							nbVars = int.Parse(splitedString[1]);
							ViewBag.nbVars = nbVars;
							continue;
						}
						if (splitedString[0] == "nbCons"){
							nbCons = int.Parse(splitedString[1]);
							ViewBag.nbCons = nbCons;
							continue;
						}

						if (splitedString[0] == "function"){
							func = new ClassX[nbVars];
							line = sr.ReadLine();
							splitedString = line.Split(':');
							for (int j=0; j<nbVars;j++){
								var x = new ClassX("x",j+1,int.Parse(splitedString[j]));
								func[j]=x;
							}
							continue;
						}

						if (splitedString[0] == "constraints"){
							constraints = new List<ClassConstraint>();
							for (int i=0; i<nbCons; i++){
								var constraint = new ClassConstraint();
								line = sr.ReadLine();
								splitedString = line.Split(':');
								ClassX x;
								for (int j=0; j<nbVars;j++){
									x = new ClassX("x", j+1, int.Parse(splitedString[j]));
									constraint.LeftMember.Add(x);
								}
								constraint.Operator = splitedString[nbVars];
								x = new ClassX("b",0,int.Parse(splitedString[nbVars+1]));
								constraint.RightMember.Add(x);
								constraints.Add(constraint);
							}
							continue;
						}

						if (splitedString[0] == "conditions"){
							conditions = new List<ClassConstraint>();
							for (int i=0; i<nbVars; i++){
								var condition = new ClassConstraint();
								line = sr.ReadLine();
								splitedString = line.Split(':');

								condition.Option = splitedString[0];

								var x = new ClassX("x", i+1, 1);
								condition.LeftMember.Add(x);
								condition.Operator = splitedString[1];
								x = new ClassX("b",0,0);
								condition.RightMember.Add(x);

								conditions.Add(condition);
							}
							continue;
						}

					} //while

					display();

				//	result += Function.calcul(func, tab, goal, nbVars, nbCons, nbVarsTotal);
				//	Function.persitData(currentMd5, func, tab, goal, nbVars, nbCons, nbVarsTotal);
				//	if (nbVars == 2)
				//		result += Function.generateGraphic(func, tab, goal, nbVars, nbCons, 
				//		                                   nbVarsTotal);
				}//using
			}catch{ // (Exception ex){
				//ViewBag.error = ex.Message + " T.T";
				return Redirect(Url.Action("Index", "Home"));
			}

			return View ();
		}//Index

		private void display(){
			string result = "";
			result += "<select id=\"goal\" name=\"goal\" onchange=\"refresh_graphic()\">\n";
				
			if (goal == "max"){
				result += " \t\t<option value=\"max\">Maximum</option>\n" +
					" \t\t<option value=\"min\">Minimum</option>\n";
			}else{
				result += " \t\t<option value=\"max\">Maximum</option>\n" +
					" \t\t<option value=\"min\" selected=\"selected\">Minimum</option>\n";
			}
			result += "\t</select>";

			ViewBag.selectedGoal = result;

			result = "";

			for (int i = 0; i < nbVars; i++) {
				var x = func[i];
				result += "<input type=\"number\" id=\"x"+i+"\" name=\"x"+i+"\" " +
					"value=\""+x.Coeff+"\" onchange=\"refresh_graphic()\" required/>x"+"<sub>"+(i+1)
					+"</sub>\n";
				if (i < nbVars-1)
					result += " + ";
			}

			string[] listConstraintColors = {"#4F3BFC", "#8D7768", "#F1C40F", "#2C3E50", "#00FBF2",
				"#FB00E4", "#36FB00", "#999856", "#D1D0F7", "#F7D4D0"};
			string[] listCondtionColors = {"#3DAE4A", "#1176E3", "#DEADE0", "#ADE0DF",
				"#C4E0AD", "#E0ADAD"};

			if (nbVars == 2){
				result += "<input type=\"color\" id=\"colorXY\" " +
					"name=\"colorXY\" value=\"#000000\" onchange=\"refresh_graphic()\"/>";
			}

			result += "<br/>";
			ViewBag.formula1 = result;

			result = "";

			for (int i = 0; i < nbCons; i++) {
				result += "<tr id=\"row"+i+"\">";
				result += "<th><input type=\"button\" id=\"btnDel"+i+"\" name=\"btnDel"+i+"\" " +
					" class=\"btnDel\" " +
					"onclick=\"del_row('"+i+"')\"/>";
				result += "</th>\n";

				var constraint = constraints[i];

				for (int j = 1; j < nbVars+1; j++) {
					var x = constraint.LeftMember[j-1];
					result += "<th><input type=\"number\" " +
						"id=\"c"+i+"x"+j+"\" " +
						"name=\"c"+i+"x"+j+"\" " +
						"value=\""+x.Coeff+"\" onchange=\"refresh_graphic()\" required/>x<sub>"+j+
						"</sub>\n";
					result += "</th>\n";
					if (j < nbVars)
						result += "<th>+</th>\n";
				}

				result += "<th>";
				result += "<select id=\"o"+i+"\" name=\"o" + i + "\"" +
					" onchange=\"refresh_graphic()\"\n>";

				if (constraint.Operator == "<="){
					result+= "<option value=\"<=\" selected=\"selected\">&le;</option>"
						+ "<option value=\">=\">&ge;</option>"
						+ "<option value=\"=\">=</option>";
				}else if (constraint.Operator == ">="){
					result+= "<option value=\"<=\">&le;</option>"
						+ "<option value=\">=\" selected=\"selected\">&ge;</option>"
						+ "<option value=\"=\">=</option>";
				}else{
					result+= "<option value=\"<=\">&le;</option>"
						+ "<option value=\">=\">&ge;</option>"
						+ "<option value=\"=\" selected=\"selected\">=</option>";
				}
				
				result += "</select>\n";
				result += "</th>\n";
				result += "<th>";
				result += "<input type=\"number\" id=\"c"+i+"x0\" " 
					+"name=\"c"+i+"x0\" value=\""+constraint.RightMember[0].Coeff
				    +"\" onchange=\"refresh_graphic()\" required/>";
				result += "</th>\n";

				if (nbVars == 2){
					result += "<th>";
					result += "<input type=\"color\" id=\"color"+i
						+"\" name=\"color"+i;

					if (i < listConstraintColors.Length){
						result += "\" value=\""+listConstraintColors[i]+"\"";
					}else{
						result += "\" value=\"#ff0000\"";
					}

					result += " onchange=\"refresh_graphic()\" />";
					result += "</th>";
				}

				result += "</tr>\n";

				if (i == nbCons-1){
					result += "<tr>";
					i++;
					result += "<th><input type=\"button\" id=\"new_btnAdd\" name=\"new_btnAdd\" " +
						" class=\"btnAdd\"" +
						"onclick=\"add_row()\"/></th>\n";
					for (int j = 1; j < nbVars+1; j++) {
						result += "<th><input type=\"number\" class=\"newAdd\" " +
							"id=\"new_cx"+j+"\" " +
							"name=\"new_cx"+j+"\" " +
							"value=\"1\" />x<sub>"+j;
						result += "</sub></th>\n";
						if (j < nbVars)
							result += "<th>+</th>\n";
					}

					result += "<th><select class=\"newAdd\" id=\"new_o\" name=\"new_o\">\n"
						+ "<option value=\"<=\">&le;</option>"
						+ "<option value=\">=\">&ge;</option>"
						+ "<option value=\"=\">=</option>"
						+ "</select>\n";
					result += "</th>\n";
					result += "<th><input type=\"number\" class=\"newAdd\" " +
						"id=\"new_cx0\" " +
						"name=\"new_cx0\" value=\"1\" />\n";
					result += "</th>\n";

					if (nbVars == 2){
						result += "<th>";
						result += "<input type=\"color\" class=\"newAdd\" id=\"new_color\" " +
							"name=\"new_color\" value=\"#ff0000\"/>";
						result += "</th>";
					}

					result += "</tr>\n";
				}
			}

			ViewBag.formula2 = result;

			result = "<h4>AVEC</h4>\n";
			result += "<table  cellspacing=2 cellpadding=2 id=\"data_table_cond\" border=0>\n";

			for (int i = 0; i < nbVars; i++) {
				result += "<tr>\n";

				result += "<th>";
				/*	result += "<input type=\"number\" " +
						"id=\"cond"+i+"x\" " +
						"name=\"cond"+i+"x\" " +
						"value=\"1\" required/>x<sub>"+(i+1);
					result += "</sub>";*/
				var condition = conditions[i];

				result += "<select id=\"cond"+i+"\" name=\"cond" + i + "\"" +
					" onchange=\"refresh_graphic()\" >\n";
				
				if (condition.Option == "sans"){
					result += "<option value=\"avec\">Avec</option>"
						+ "<option value=\"sans\" selected=\"selected\">Sans</option>";
				}else{
					result += "<option value=\"avec\" selected=\"selected\">Avec</option>"
						+ "<option value=\"sans\">Sans</option>";
				}
				
				result += "</select>\n";
				result += "x<sub>"+(i+1);
				result += "</th>\n";

				result += "<th>";
				result += "<select id=\"cond"+i+"o\" name=\"cond" + i + "o\"" +
					" onchange=\"refresh_graphic()\" >\n";

				if (condition.Operator == "<="){
					result+= "<option value=\"<=\" selected=\"selected\">&le;</option>"
						+ "<option value=\">=\">&ge;</option>";
				}else{
					result+= "<option value=\"<=\">&le;</option>"
						+ "<option value=\">=\" selected=\"selected\">&ge;</option>";
				}

			/*	result += "<option value=\">=\">&ge;</option>"
					+ "<option value=\"<=\">&le;</option>";
					*/
				result += "</select>\n";
				result += "</th>\n";

				result += "<th>";
				/*	result += "<input type=\"number\" id=\"cond"+i+"x0\" " +
						"name=\"cond"+i+"x0\" value=\"0\" " +
						"onchange=\"refresh_graphic()\" required/>";
						*/
				result += 0;
				result += "</th>\n";

				if (nbVars == 2){
					result += "<th>";
					result += "<input type=\"color\" id=\"cond"+i+"color\" " +
						"name=\"cond"+i+"color\" ";

					if (i < listCondtionColors.Length){
						result += "value=\""+listCondtionColors[i]+"\"";
					}else{
						result += "value=\"#ff0000\"";
					}
					result += " onchange=\"refresh_graphic()\" />";
					result += "</th>\n";
				}

				result += "</tr>\n";
			}

			result += "</table>\n";


			ViewBag.Formula3 = result;

			result = "";

			if (nbVars == 2){
				result = "<div id=\"box\" class=\"jxgbox\" style=\"width:800px; " +
					"height:600px;\"></div>\n" +
					"\t<script type=\"text/javascript\">\n ";
				result += "refresh_graphic();\n";
				result += "\t</script>";
			}//if

			ViewBag.graphic = result;
		}
    }
}
