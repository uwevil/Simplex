﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace SimplexMVC.Controllers
{
	public class HomeController : Controller
	{
		private string currentMd5 ;

		public ActionResult Index()
		{
			currentMd5 = (new HashMd5()).get();
			ViewBag.currentMd5 = currentMd5;
			return View();
		}
	}
}
