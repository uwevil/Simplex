﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimplexMVC.Controllers
{
    public class SolutionPageController : Controller
    {
		private string currentMd5;
        
		public ActionResult Index()
        {
			string result = "";
			try{
				string goal = Request.Form["goal"];
				int nbVars = int.Parse(Request.Form["nbVars"]);
				int nbCons = int.Parse(Request.Form["nbCons"]);
				currentMd5 = Request.Form["checked"];

				ViewBag.nbVars = nbVars;
				ViewBag.nbCons = nbCons;
				ViewBag.currentMd5 = currentMd5;

				int currentRowIndex = 0;
				int maxIndex = 0;

				/*
			* inputTargetFunction = ClassContstraint
			* 	LeftMember = List<ClassX>
			* 	Operator = null (empty but not null)
			* 	RightMember = null (empty but not null)
			* 
			* inputConstraints = List<ClassConstraint>
			* 	0 : Constraint 1
			* 	1 : Constraint 2
			* 	n : Constraint m+1
			* In each constraint:
			* 	LeftMember : x1 x2 ... xn
			* 	RightMember : b
			* 	Operator : '>=' or '<=' or '='
			* 
			*/
				var inputTargetFunction = new ClassConstraint();
				var inputConstraints = new List<ClassConstraint>();

				for (int i=0; i<nbVars; i++){
					var xTmp = int.Parse(Request.Form["x"+i]);
					inputTargetFunction.LeftMember.Add(new ClassX("x", i+1, xTmp));
				}

				for (int i=0; i<nbCons; i++){
					string operatorTmp ;
					while (true){
						if (Request.Form["o"+currentRowIndex] != null)
							break;
						currentRowIndex++;
					}

					var constraint = new ClassConstraint();

					operatorTmp = Request.Form["o"+currentRowIndex];

					constraint.Operator = operatorTmp;

					for (int j=0; j<=nbVars; j++){
						int coeff = int.Parse(Request.Form["c"+currentRowIndex+"x"+j]);
						if (j == 0){
							constraint.RightMember.Add(new ClassX("b", 0, coeff));
						}else{
							constraint.LeftMember.Add(new ClassX("x", j, coeff));
							maxIndex = j;
						}						
					}

					inputConstraints.Add(constraint);
					currentRowIndex++;
				}

				/*
			* inputConditions : List<ClassConstraint>
			* 	Option : sans or avec
			* 	LeftMember : xi (Count = 1)
			* 	Operator : not empty ('<=' or '>=')
			* 	RightMember : not empty but (Count >= 1)
			*/
				var inputConditions = new List<ClassConstraint>();

				for (int i=0; i<nbVars; i++){
					var condition = new ClassConstraint();

					string withOrWithout = Request.Form["cond"+i]; 
					string o = Request.Form["cond"+i+"o"];

					condition.Option = withOrWithout;
					condition.Operator = o;
					condition.RightMember.Add(new ClassX("b", 0, 0));//x0));
					condition.LeftMember.Add(new ClassX("x", i+1, 1));

					inputConditions.Add(condition);
				}//for

				/*
			* Persist data to download
			*/
				Function.PersistData(currentMd5, goal, nbVars, nbCons, inputTargetFunction, 
				                 inputConstraints, inputConditions);

				/*
			* outputCOnstraints : a deep copy of inputConstraints
			*/
				var outputConstraints = new List<ClassConstraint>(inputConstraints.Count());
				inputConstraints.ForEach((item) => {
					outputConstraints.Add(item.Clone());
				});

				/*
				* Initial function, constraints and conditions
				*/
			/*	ClassConstraint inputTargetFunctionInit = inputTargetFunction.Clone();
				List<ClassConstraint> inputConstraintsInit = 
					new List<ClassConstraint>(inputConstraints.Count);
				inputConstraints.ForEach((item) => {
					inputConstraintsInit.Add(item.Clone());
				});
				List<ClassConstraint> inputConditionsInit = 
					new List<ClassConstraint>(inputConditions.Count);
				inputConditions.ForEach((item) => {
					inputConditionsInit.Add(item.Clone());
				});
				*/
				result = "";
				result += printInit(goal, inputTargetFunction, inputConstraints, inputConditions);

				if (goal == "min"){
					result += "<h5>Si la fonction objective est de calculer le minimum," +
						" on la multiplie par -1.</h5>";
					result += "<h5>Mainteant, le problème revient à calculer le maximum.</h5>";
					inputTargetFunction.Multiply(-1);

					result += printInit("max", inputTargetFunction, inputConstraints, inputConditions);
				}

				ViewBag.init = result;

				/*
			* Change all constraints with type '>=' to '<=' by multiply by -1 
			*/
				for (int i=0; i<outputConstraints.Count(); i++){
					var constraint = outputConstraints[i];
					if (constraint.Operator == ">="){
						constraint.Multiply(-1);
					}
				}

				result = "";
				result += firstStep(inputTargetFunction, null, 
				                inputConstraints, outputConstraints, inputConditions);

				ViewBag.firstStep = result;

				//second step
				inputConstraints = new List<ClassConstraint>();
				outputConstraints.ForEach((item) => {
					inputConstraints.Add(item.Clone());
				});

				var outputTargetFunction = new ClassConstraint();
				outputTargetFunction = inputTargetFunction.Clone();

				/*
			* Example : 
			* avec:
			* 	if x1 >= 0, do nothing
			* 	if x1 <= 0, then remplace x1 by x1' with x1' = -x1 (in targetFunction 
			* 		and all constraints that have x1)
			* 		and add new condition to inputCondition 
			* sans:	
			* 	if x1 is not specified, remplace x1 : x1 = x1' - x1'' (in targetFunction 
			* 		and all constraints that have x1)
			* 		and add new conditions to inputCondition 
			*/
				int size = inputConditions.Count();
				for (int i=0; i< size; i++){
					var condition = inputConditions[i];
					if (condition.Option == "avec"){
						if (condition.Operator == ">=")
							continue;

						foreach (ClassX x in outputTargetFunction.LeftMember.ToList()){
							if (x.Index == condition.LeftMember[0].Index){
								var tmp = new ClassX(x.Name, 
								                 maxIndex+1, x.Coeff*-1);
								outputTargetFunction.LeftMember
								                .Insert(outputTargetFunction.LeftMember.IndexOf(x), 
								                        tmp);
								outputTargetFunction.LeftMember.Remove(x);
							}
						}

						foreach (ClassConstraint constraint in outputConstraints.ToList()){
							foreach(ClassX x in constraint.LeftMember.ToList()){
								if (x.Index == condition.LeftMember[0].Index){
									var tmp = new ClassX(x.Name, 
									                 maxIndex+1, x.Coeff*-1);
									constraint.LeftMember.Insert(constraint.LeftMember.IndexOf(x), 
									                             tmp);
									constraint.LeftMember.Remove(x);
								}
							} //foreach
						}//froeach

						var newCond = new ClassConstraint();
						var newX = new ClassX(condition.LeftMember[0].Name, condition.LeftMember[0].Index, 
						                  condition.LeftMember[0].Coeff);
						var newB = new ClassX(condition.LeftMember[0].Name, maxIndex+1, -1);

						newCond.Option = "avec";
						newCond.Operator = "=";
						newCond.LeftMember.Add(newX);
						newCond.RightMember.Add(newB);

						inputConditions.Add(newCond);

						newCond = new ClassConstraint();
						newX = new ClassX(condition.LeftMember[0].Name, maxIndex+1, 1);
						newB = new ClassX(condition.RightMember[0].Name, 
						              condition.RightMember[0].Index, condition.RightMember[0].Coeff*-1);

						newCond.Option = "avec";
						newCond.Operator = ">=";
						newCond.LeftMember.Add(newX);
						newCond.RightMember.Add(newB);
						inputConditions.Add(newCond);

						maxIndex++;
					}else{
						foreach (ClassX x in outputTargetFunction.LeftMember.ToList()){
							if (x.Index == condition.LeftMember[0].Index){
								var tmp = new ClassX(x.Name, 
								                 maxIndex+1, x.Coeff);
								outputTargetFunction.LeftMember
								                .Insert(outputTargetFunction.LeftMember.IndexOf(x), tmp);
								tmp = new ClassX(x.Name, 
								             maxIndex+2, x.Coeff*-1);
								outputTargetFunction.LeftMember
								                .Insert(outputTargetFunction.LeftMember.IndexOf(x), tmp);
								outputTargetFunction.LeftMember.Remove(x);
							}
						}

						foreach (ClassConstraint constraint in outputConstraints.ToList()){
							foreach(ClassX x in constraint.LeftMember.ToList()){
								if (x.Index == condition.LeftMember[0].Index){
									var tmp = new ClassX(x.Name, 
									                 maxIndex+1, x.Coeff);
									constraint.LeftMember.Insert(constraint.LeftMember.IndexOf(x), tmp);

									tmp = new ClassX(x.Name, 
									             maxIndex+2, x.Coeff*-1);
									constraint.LeftMember.Insert(constraint.LeftMember.IndexOf(x), tmp);
									constraint.LeftMember.Remove(x);
								}
							} //foreach

						}//froeach

						var newCond = new ClassConstraint();
						var newX1 = new ClassX(condition.LeftMember[0].Name, maxIndex+1, 1);
						var newX2 = new ClassX(condition.LeftMember[0].Name, maxIndex+2, -1);
						var newB1 = condition.LeftMember[0].Clone();

						newCond.Option = "avec";
						newCond.Operator = "=";
						newCond.LeftMember.Add(newB1);
						newCond.RightMember.Add(newX1);
						newCond.RightMember.Add(newX2);

						inputConditions.Add(newCond);

						newCond = new ClassConstraint();
						newX1 = newX1.Clone();
						newB1 = new ClassX(condition.RightMember[0].Name, 
						               condition.RightMember[0].Index, condition.RightMember[0].Coeff*-1);

						newCond.Option = "avec";
						newCond.Operator = ">=";
						newCond.LeftMember.Add(newX1);
						newCond.RightMember.Add(newB1);
						inputConditions.Add(newCond);

						newCond = new ClassConstraint();
						newX2 = newX2.Clone();
						var newB2 = new ClassX(condition.RightMember[0].Name, 
						                   condition.RightMember[0].Index, condition.RightMember[0].Coeff*-1);

						newCond.Option = "avec";
						newCond.Operator = ">=";
						newCond.LeftMember.Add(newX2);
						newCond.RightMember.Add(newB2);
						inputConditions.Add(newCond);

						maxIndex += 2;
					}//if == sans/avec
				} //big for

				result = "";
				result += firstStep(inputTargetFunction, outputTargetFunction, 
				                inputConstraints, outputConstraints, inputConditions);

				ViewBag.secondStep = result;

				//third step
				inputConstraints = new List<ClassConstraint>();
				outputConstraints.ForEach((item) => {
					inputConstraints.Add(item.Clone());
				});

				/*
			* change all inegality constraints to egality constraints by adding ecart variables
			* 
			* NOTE : included all constraints even if a constraint has type '='
			* 
			* Example :
			* 	ax - by +cz = -12 ---> ax - by + cz + e1 = -12
			* 	ax + by - cz <= 1 ---> ax + by - cz + e2 = 1
			* 	ax - by - cz >= 12 ---> ax - by - cz + e3 = 12 
			*/

				{
					int i = 1;
					foreach (ClassConstraint constraint in outputConstraints){
						constraint.Operator = "=";
						var diff = new ClassX("e", i++, 1);
						constraint.LeftMember.Add(diff);

						var cond = new ClassConstraint();
						cond.Operator = ">=";
						cond.LeftMember.Add(diff.Clone());
						cond.RightMember.Add(new ClassX("b", 0, 0));
						inputConditions.Add(cond);
					}
				}

				result = "";
				result += firstStep(outputTargetFunction, null,
				                inputConstraints, outputConstraints, inputConditions);

				ViewBag.thirdStep = result;

				//fourth step
				inputConstraints = new List<ClassConstraint>();
				outputConstraints.ForEach((item) => {
					inputConstraints.Add(item.Clone());
				});

				/*
			* transform all constraints that have b (independent term) < 0 to having b >= 0 
			* by multiply by -1
			*/

				foreach (ClassConstraint constraint in outputConstraints){
					if (constraint.RightMember[0].Coeff < 0){
						constraint.RightMember[0].Multiply(-1);
						foreach(ClassX x in constraint.LeftMember){
							x.Multiply(-1);
						}
					}
				}

				result = "";
				result += firstStep(outputTargetFunction, null, 
				                inputConstraints, outputConstraints, inputConditions);

				ViewBag.fourthStep = result;

				/*
				* canonical form
				*/
				result = "";
				result += printInit(goal, outputTargetFunction, outputConstraints, inputConditions);

				ViewBag.canonical = result;


				//fifth step(solution)
				/*
			* nbVarsTotal = all members of LeftMember 
			* 				- 1(except ecart variable)
			* 				+ number of constraints(number of ecart variables) 
			* 				+ 1(a place for storing b)
			* 
			* fTab constains array[nb of constraints, nbVarsTotal]
			* 	0 : x1 x2 x3 ... xn e1 e2 ... em b0
			* 	1 : x1 x2 x3 ... xn e1 e2 ... em b1
			* 	  .	  .	  .	  . 
			* 	  .	  .	  .	  .	
			* 	  .	  .   .	  .	
			* 	m : x1 x2 x3 ... xn e1 e2 ... em bm
			* 
			* fFunc : array[nbVarsTotal]
			* 	x1 x2 ... xn e1 e2 ... em b
			*/
				int nbVarsTotal = outputConstraints[0].LeftMember.Count-1+nbCons+1;
				ClassX[,] fTab = new ClassX[nbCons, nbVarsTotal];
				ClassX[] fFunc = new ClassX[nbVarsTotal];

				/*
			* if min ---> fFunc = -1 * fFunc
			* 
			* copy to fFunc
			*/
				{
					for(int i=0; i<outputTargetFunction.LeftMember.Count; i++){
						var x = outputTargetFunction.LeftMember[i].Clone();
					//	if (goal == "min"){
					//		x.Multiply(-1);
					//		fFunc[i] = x;
					//	}else{
							fFunc[i] = x;
					//	}
					}
					for (int i=0; i<nbCons; i++){
						fFunc[outputTargetFunction.LeftMember.Count+i] = new ClassX("e", i+1, 0);
					}
					fFunc[nbVarsTotal-1] = new ClassX("b", 0, 0);
				}

				/*
			* copy to fTab
			*/
				for (int j=0; j<outputConstraints[0].LeftMember.Count-1; j++){
					for(int i=0; i<nbCons; i++){
						var x = outputConstraints[i].LeftMember[j].Clone();
						fTab[i,j] = x;
					}
				}

				{
					int sizeTmp = outputConstraints[0].LeftMember.Count-1;
					for (int j=sizeTmp; j<nbVarsTotal; j++){
						for (int i = 0; i<nbCons; i++){
							if (j < nbVarsTotal-1){
								var x = outputConstraints[i].LeftMember[sizeTmp];
								if (x.Index != j-sizeTmp+1){
									x = new ClassX("e", j+1-sizeTmp, 0);
									fTab[i,j] = x;
								}else{
									fTab[i,j] = x.Clone();
								}
							}else{
								var x = new ClassX("b", 0, 
								               outputConstraints[i].RightMember[0].Coeff).Clone();
								fTab[i,j] = x;
							}
						}
					}
				}

				result = "";

				result += "<h4>étape 0</h4>";
				result += "<h5>Construire le premier tableau correspondant à la forme canonique</h5>";
				result += printTable(fFunc, fTab, goal, -1, -1, null, null, inputConditions);

				var listIndexMin = new List<int>();
				var listIndexMax = new List<int>();
				int step = 1;
				int indexMax = -1;
				int indexMin = -1;

				/*
				* isConstinue = 
				* 	0 = continue
				* 	1 = optimum
				* 	2 = no solution
				*/
				int isContinue = 0;
				while(isContinue==0){
					indexMax = -1;
					indexMin = -1;
					foreach (int i in Function.IndexMax(fFunc)){
						if (i < 0)
							break;
						indexMax = i;
						indexMin = Function.IndexMin(fTab, indexMax);
						if (indexMin < 0)
							continue;
						if (indexMax >= 0 && indexMin >= 0)
							break;
					}

					if (indexMax == -1 || indexMin == -1){
						isContinue = 2;
						break;
					}

					listIndexMin.Add(indexMin);
					listIndexMax.Add(indexMax);

					ClassX[,] newFTab = Function.CalculTab(fTab, indexMin, indexMax);
					ClassX[] newFFunc; 

					isContinue = Function.CalculFunc(out newFFunc, fTab, fFunc, indexMin, indexMax); 
			
					result += "<h4>étape "+(step++)+"</h4>";
					result += printInstructions(fFunc, fTab, indexMin, indexMax);

					fTab = newFTab;
					fFunc = newFFunc;
					result += "<h5>Voici le résultat</h5>\n";
					result += printTable(fFunc, fTab, goal, 
					                     indexMin, indexMax, listIndexMin, listIndexMax, 
					                     inputConditions);
					result += "<h5>Les coefficients de la fonction objective " +
						"sont tous nuls ou négatifs? (si oui, nous sommes à l'optimum, " +
						"si non nous effectuons un nouveau passage)</h5>";
				}

				result += "<h4>étape finale</h4>";
				result += printTable(fFunc, fTab, goal, 
				                     -1, -1, listIndexMin, listIndexMax, inputConditions);
				
				if (isContinue == 1){
					result += "<h5>Les coefficients de la fonction économique sont tous nuls ou négatifs, " +
						"fin de l'algorithme du simplex.</h5>";
					result += "<h5>La solution qui rend optimal le programme " +
						"de  production est le suivant :</h5>";
					if (goal == "min"){
						result += "<h6>La marge sur coût variable minimum = "
							+fFunc[fFunc.Length-1].Fraction*-1+"</h6>";
					}else{
						result += "<h6>La marge sur coût variable maximum = "
							+fFunc[fFunc.Length-1].Fraction*-1+" ";
					}
					result += "avec (";

				/*	for (int i=0; i<listIndexMin.Count; i++){
						result += i+",";
						result += listIndexMin[i]+",";
						result += listIndexMax[i]+"<br/>";
					}
*/
					for (int i=0; i<fFunc.Length-1; i++){
						if (listIndexMax.Contains(i)){
							var x = fTab[listIndexMin[listIndexMax.IndexOf(i)], 
							         fTab.GetLength(1)-1];
							result += ""+x.Fraction;
						}else{
							result += "0";
						}
						if (i < fFunc.Length-1-1)
							result += ", ";
					}
					result += ")</h6>";
				}else if (isContinue == 2){
					result += "<h5>La ligne \"c\" comporte des éléments strictement positifs " +
						"mais aucune des colonnes au dessus de ces éléments ne comporte " +
						"un coefficient strictement positifs. Dans ce cas, la fonction objective " +
						"n'est pas bornée.</h5>";
				}
				result += printInit(goal, outputTargetFunction, outputConstraints, inputConditions);

				ViewBag.solution = result;
			}catch{ //(Exception e){
				return Redirect(Url.Action("Index", "Home"));
			}

			return View ();
        }//Index

		internal string printFunction(ClassConstraint inputTargetFunction){
			string result = "";
			for (int i=0; i<inputTargetFunction.LeftMember.Count(); i++){
				var x = inputTargetFunction.LeftMember[i];
				var tmp = x.Coeff;
				if (i==0)
					result += tmp+x.Name+"<sub>"+x.Index+"</sub>";
				else{
					if (tmp >= 0){
						result += " + " + tmp+x.Name+"<sub>"+x.Index+"</sub>";
					}else{
						result += " - " + (tmp*-1) +x.Name+"<sub>"+x.Index+"</sub>";
					}
				}
			}

			return result;
		}

		internal string printInit(string goal, ClassConstraint inputTargetFunction,
		                                 List<ClassConstraint> inputConstraints, 
		                                 List<ClassConstraint> inputConditions){
			string result = "";
			result += "<table class=\"table-fill\" border=\"2\" ><tr>\n";
			result += "<th class=\"text-center\">Fonction objective "+goal+"</th>\n";

			result += "<td class=\"text-center\">\n";
			result += printFunction(inputTargetFunction);
			result += "</td>\n";
			result += "</tr>\n";

			for (int i=0;i<inputConstraints.Count(); i++){
				var constraint = inputConstraints[i];

				if (i == 0){
					result += "<tr>\n";
					result += "<th rowspan=\""+inputConstraints.Count()
					                                           +"\" class=\"text-center\"> ";
					result += "Constraintes";
					result += "</th>";
				}else{
					result += "<tr>\n";
				}

				result += "<td class=\"text-center\">";
				result += printConstraint(constraint);
				result += "</td>";

				result += "</tr>\n";
			}
			result += "<tr>";
			result += "<th class=\"text-center\"> ";
			result += "Avec";
			result += "</th>";
			result += "<td class=\"text-center\">";
			result += printConditions(inputConditions);
			result += "</td>";
			result += "</tr>\n";
			result += "</table>\n";

			return result;
		}

		internal string printConstraint(ClassConstraint constraint){
			string result = "";
			for (int j=0; j<constraint.LeftMember.Count();j++){
				var x = constraint.LeftMember[j];
				var tmp = x.Coeff;
				if (j == 0)
					result += tmp+x.Name+"<sub>"+x.Index+"</sub>";
				else{
					if (tmp >= 0){
						result += " + " + tmp+x.Name+"<sub>"+x.Index+"</sub>";
					}else{
						result += " - " + (tmp*-1) +x.Name+"<sub>"+x.Index+"</sub>";
					}
				}
			}
			result += " "+constraint.Operator+ " ";

			for (int j=0; j<constraint.RightMember.Count();j++){
				var x = constraint.RightMember[j];
				var tmp = x.Coeff;
				if (x.Name == "b"){
					result += x.Coeff;
				}else{
					if (j == 0)
						result += tmp+x.Name+"<sub>"+x.Index+"</sub>";
					else{
						if (tmp >= 0){
							result += " + " + tmp+x.Name+"<sub>"+x.Index+"</sub>";
						}else{
							result += " - " + (tmp*-1) +x.Name+"<sub>"+x.Index+"</sub>";
						}
					}
				}
			}

			return result;
		}

		internal string printConditions(List<ClassConstraint> inputConditions){
			string result = "";

			foreach (ClassConstraint constraint in inputConditions){
				if (constraint.Option == "sans")
					continue;
				if (result.Length>0)
					result += ", ";
				var leftMember = constraint.LeftMember[0];
				var rightMember = constraint.RightMember;
				result += leftMember.Name+"<sub>"+leftMember.Index+"</sub> "
				                    + constraint.Operator + " ";

				for (int j=0; j<constraint.RightMember.Count();j++){
					var x = constraint.RightMember[j];
					var tmp = x.Coeff;
					if (x.Name == "b"){
						result += x.Coeff;
					}else{
						if (j == 0)
							result += tmp+x.Name+"<sub>"+x.Index+"</sub>";
						else{
							if (tmp >= 0){
								result += " + " + tmp+x.Name+"<sub>"+x.Index+"</sub>";
							}else{
								result += " - " + (tmp*-1) +x.Name+"<sub>"+x.Index+"</sub>";
							}
						}
					}
				}//for

			}//foreach

			return result;
		}

		internal string firstStep(ClassConstraint inputTargetFunction,
		                        ClassConstraint outputTargetFunction,
								List<ClassConstraint> inputConstraints, 
		                        List<ClassConstraint> outputConstraints,
		                        List<ClassConstraint> inputConditions){
			string result = "";
			result += "<table class=\"table-fill\" border=\"2\" ><tr>";
			result += "<th class=\"text-center\">Avant</th>" +
				"<th class=\"text-center\">Après</th>";
			result += "<tr/>";

			result += "<tr>";
			if (outputTargetFunction == null)
				result += "<td colspan=\"2\" class=\"text-center\"> ";
			else
				result += "<td class=\"text-center\"> ";

			result += printFunction(inputTargetFunction);

			result += "</td>";

			if (outputTargetFunction != null){
				result += "<td class=\"text-center\"> ";
				result += printFunction(outputTargetFunction);
				result += "</td>";
			}
			result += "</tr>\n";

			//debug
		/*	foreach (ClassConstraint constraint in inputConstraints){
				result += constraint.ToString();
				result += "<br/>";
			}
			result += "<br/>";
			foreach (ClassConstraint constraint in outputConstraints){
				result += constraint.ToString();
				result += "<br/>";
			}
			//end debug
			*/

			for (int i=0;i<inputConstraints.Count(); i++){
				var constraint = inputConstraints[i];
				result += "<tr>";

				result += "<td class=\"text-center\">";
				result += printConstraint(constraint);
				result += "</td>";

				result += "<td class=\"text-center\">";
				constraint = outputConstraints[i];
				result += printConstraint(constraint);
				result += "</td>";
				result += "</tr>\n";
			}
			result += "</table>\n";
			result += "AVEC<br/>\n";
			result += printConditions(inputConditions);

			return result;
		} // firstStep

		internal string printInstructions(ClassX[] fFunc, ClassX[,] fTab, 
		                                  int indexMin, int indexMax){
			string result ="";
			var pivot = fTab[indexMin, indexMax];
			result += "<h5>Le coefficient de la fonction objective est "
				+fFunc[indexMax].Fraction.ToString()+". Ainsi il s'agit de la variable "
			                    +pivot.Name+"<sub>"+pivot.Index+"</sub> qui rentre en base.</h5>\n";
			result += "<h5>Choisir la variable à enlever de la base " +
				"(rapport: b / coefficient de la variable choisie). " +
				"Retenir le plus faible. Ici, e"+(indexMin+1)+" est remplacée par " +
				pivot.Name+"<sub>"+pivot.Index+"</sub> (couleur verte).</h5>\n";
			result += "<h5>Le pivot est égal à "+pivot.Fraction+".</h5>\n";
			result += "<h5>Multiplier la ligne du pivot par le rapport : " +
				"1 / valeur du pivot (ou diviser la ligne du pivot par le pivot).</h5>\n";
			result += "<h5>Calculer les valeurs des autres lignes en appliquant : </h5>\n";

			result += "<table class=\"table-fill2\" border=\"2\" >\n"; // class=\"table-fill2\" ><tr>\n";

			result += "<tr>\n";
			for (int i=0; i<fTab.GetLength(0); i++){
				if (i != indexMin){
					result += "<th>Ligne "+(i+1)+"</th>\n";
				}
			}
			result += "</tr>\n";

			for (int j=0; j<fTab.GetLength(1); j++){
				result += "<tr>\n";
				for (int i=0; i<fTab.GetLength(0); i++){
				//	if (i == indexMin && j == indexMax){
				//		var x = fTab[indexMin, indexMax];
						//x.Name, x.Index, 1);
				//	}else if (i == indexMin){
				//		var x = fTab[i,j].Clone();
				//		x.Fraction = x.Fraction / pivot.Fraction;
				//		newFTab[i, j] = x;
				//	}else 
					if (i != indexMin){
						result += "<td>";

						var x = fTab[i,j].Clone();
						var x0 = fTab[i,j].Clone();
						var frac = fTab[i, indexMax].Fraction/pivot.Fraction;
						frac = frac * fTab[indexMin, j].Fraction;
						x.Fraction = x.Fraction - frac;

						if (x.Fraction.Denominator != 1){
							result += "<sup>"+x.Fraction.Numerator+"</sup>&frasl;<sub>"
							                   +x.Fraction.Denominator + "</sub>";
						}else{
							result += (decimal)x.Fraction;
						}
						result += " = ";
						if (x0.Fraction.Denominator != 1){
							result += "<sup>"+x0.Fraction.Numerator+"</sup>&frasl;<sub>"
							                    +x0.Fraction.Denominator+"</sub>";
						}else{
							result += (decimal)x0.Fraction;
						}

						result += " - ((";
						if (fTab[i, indexMax].Fraction.Denominator != 1){
							result += "<sup>"+fTab[i, indexMax].Fraction.Numerator 
							          +"</sup>&frasl;<sub>"
							       +fTab[i, indexMax].Fraction.Denominator+"</sub>";
						}else{
							result += (decimal)fTab[i, indexMax].Fraction;
						}

						result += "/";
						if (pivot.Fraction.Denominator != 1){
							result += "<sup>"+pivot.Fraction.Numerator+"</sup>&frasl;<sub>"
							                       +pivot.Fraction.Denominator+"</sub>";
						}else{
							result += (decimal)pivot.Fraction;
						}

						result += ")*";
						if (fTab[indexMin,j].Fraction.Denominator != 1){
							result += "<sup>" + fTab[indexMin,j].Fraction.Numerator
							 +"</sup>&frasl;<sub>"+
							fTab[indexMin,j].Fraction.Denominator+"</sub>)";
						}else{
							result += (decimal)fTab[indexMin,j].Fraction;
						}
												
						result += "</td>\n";
					}
				}
				result += "</tr>\n";
			}

			result += "</table>\n";

			return result;
		}

		internal string printTable(ClassX[] fFunc, ClassX[,] fTab, string goal, 
		                  		int indexMin, int indexMax, 
		                           List<int> listIndexMin, List<int> listIndexMax,
		                           List<ClassConstraint> inputConditions){
			string result ="";
			result += "<table class=\"table-fill2\" ><tr>\n";
			result += "<th></th>\n";

			for (int j = 0; j < fFunc.Length; j++) {
				var x = fFunc[j];
				if (j == fFunc.Length-1){
					result += "<th class=\"text-center\">"+x.Name+"</th>\n";
				}else{
					result += "<th class=\"text-center\">"+x.Name+"<sub>"
					                                        +x.Index+"</sub>"+"</th>\n";
				}
			}

			result += "</tr>\n";

			for (int i = 0; i < fTab.GetLength(0); i++){
				if (indexMin == i) {
					var x = fTab[indexMin, indexMax];
					result += "<tr id=\"tr1\"><th class=\"text-center\" id=\"td3\">\n"
						+x.Name+"<sub>"+x.Index+"</sub>"+"</th>\n";		
				}else if (listIndexMin != null && listIndexMin.Contains(i)){
					var x = fTab[listIndexMin[listIndexMin.IndexOf(i)], 
					             listIndexMax[listIndexMin.IndexOf(i)]];
					result += "<tr id=\"tr1\">\n" +
						"<th class=\"text-center\" id=\"td3\">"+x.Name
					       +"<sub>"+x.Index+"</sub>"+"</th>\n";
				}else{
					result += "<tr>\n<th class=\"text-center\">e"+(i+1)+"</th>\n";
				}

				for (int j = 0; j < fTab.GetLength(1); j++){
					if (j == indexMax && i == indexMin)
						result += "<td class=\"text-center\" id=\"td1\" >" 
							+ fTab[i, j].Fraction + "</td>\n";
					else if (j == indexMax || i == indexMin)
						result += "<td class=\"text-center\" id=\"td2\" >" 
							+ fTab[i, j].Fraction + "</td>\n";
					else if (j == fTab.GetLength(1)-1 && listIndexMin != null 
					         && listIndexMin.Contains(i))
						result += "<td class=\"text-center\" id=\"td3\" >" 
							+ fTab[i, j].Fraction + "</td>\n";
					else
						result += "<td class=\"text-center\">" + fTab[i, j].Fraction + "</td>\n";
				}
				result += "</tr>\n";
			}
			result += "<tr>\n<th class=\"text-center\">c</th>\n";

			for (int i = 0; i < fFunc.Length; i++) {
				var x = fFunc[i];
				if (i == fFunc.Length-1){
					result += "<td class=\"text-center\" id=\"td4\" >" + x.Fraction + "</td>\n";
					continue;
				}
				result += "<td class=\"text-center\">" + x.Fraction + "</td>\n";
			}

			result += "</tr>\n";
			result += "</table>\n";
			result += "AVEC<br/>\n";
			result += printConditions(inputConditions);;

			return result;
		}

		public ActionResult Download(){
			currentMd5 = Request.Form["checked"];

			var dir = Path.GetTempPath();
			var fileName = "download_"+currentMd5+".txt";
			var fullPath = dir + fileName;

			byte[] fileBytes = Function.GetFile(fullPath);

			return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "save.txt");
		}//download

    }
}
