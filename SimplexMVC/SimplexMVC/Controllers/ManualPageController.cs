﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimplexMVC.Controllers
{
    public class ManualPageController : Controller
    {
		private int nbVars;
		private int nbCons;
		private string currentMd5;


        public ActionResult Index(){
			try{
				nbVars = int.Parse(Request.Form["nbVariablesText"]);
				nbCons = 3;	
				currentMd5 = Request.Form["checked"];

				if (nbVars <= 0 || nbCons <= 0)
					return Redirect(Url.Action("Index", "Home"));

				ViewBag.nbVars = nbVars;
				ViewBag.nbCons = nbCons;
				ViewBag.currentMd5 = currentMd5;

				string result = "";

				for (int i = 0; i < nbVars; i++) {
					result += "<input type=\"number\" id=\"x"+i+"\" name=\"x"+i+"\" " +
						"value=\"1\" onchange=\"refresh_graphic()\" required/>x"+"<sub>"+(i+1)
							+"</sub>\n";
					if (i < nbVars-1)
						result += " + ";
				}

				string[] listConstraintColors = {"#4F3BFC", "#8D7768", "#F1C40F", "#2C3E50", "#00FBF2",
					"#FB00E4", "#36FB00", "#999856", "#D1D0F7", "#F7D4D0"};
				string[] listCondtionColors = {"#3DAE4A", "#1176E3", "#DB11E3", "#DEADE0", "#ADE0DF",
					"#C4E0AD", "#E0ADAD"};

				if (nbVars == 2)
					result += "<input type=\"color\" id=\"colorXY\" " +
						"name=\"colorXY\" value=\"#000000\" onchange=\"refresh_graphic()\"/>";

				result += "<br/>";
				ViewBag.formula1 = result;

				result = "";

				for (int i = 0; i < nbCons; i++) {
					result += "<tr id=\"row"+i+"\">";
					result += "<th><input type=\"button\" id=\"btnDel"+i+"\" name=\"btnDel"+i+"\" " +
						" class=\"btnDel\" " +
						"onclick=\"del_row('"+i+"')\"/>";
					result += "</th>\n";

					for (int j = 1; j < nbVars+1; j++) {
						result += "<th><input type=\"number\" " +
							"id=\"c"+i+"x"+j+"\" " +
							"name=\"c"+i+"x"+j+"\" " +
							"value=\"1\" onchange=\"refresh_graphic()\" required/>x<sub>"+j+
							"</sub>\n";
						result += "</th>\n";
						if (j < nbVars)
							result += "<th>+</th>\n";
					}

					result += "<th>";
					result += "<select id=\"o"+i+"\" name=\"o" + i + "\"" +
						" onchange=\"refresh_graphic()\"\n>"
						+ "<option value=\"<=\">&le;</option>"
						+ "<option value=\">=\">&ge;</option>"
						+ "<option value=\"=\">=</option>"
					+ "</select>\n";
					result += "</th>\n";
					result += "<th>";
					result += "<input type=\"number\" id=\"c"+i+"x0\" " +
						"name=\"c"+i+"x0\" value=\"1\" onchange=\"refresh_graphic()\" required/>";
					result += "</th>\n";
					if (nbVars == 2){
						result += "<th>";
						result += "<input type=\"color\" id=\"color"+i
							+"\" name=\"color"+i;

						if (i < listConstraintColors.Length){
							result += "\" value=\""+listConstraintColors[i]+"\"";
						}else{
							result += "\" value=\"#ff0000\"";
						}

						result += " onchange=\"refresh_graphic()\" />";
						result += "</th>";
					}

					result += "</tr>\n";

					if (i == nbCons-1){
						result += "<tr>";
						i++;
						result += "<th><input type=\"button\" id=\"new_btnAdd\" name=\"new_btnAdd\" " +
							" class=\"btnAdd\"" +
							"onclick=\"add_row()\"/></th>\n";
						for (int j = 1; j < nbVars+1; j++) {
							result += "<th><input type=\"number\" class=\"newAdd\" " +
								"id=\"new_cx"+j+"\" " +
								"name=\"new_cx"+j+"\" " +
								"value=\"1\" />x<sub>"+j;
							result += "</sub></th>\n";
							if (j < nbVars)
								result += "<th>+</th>\n";
						}

						result += "<th><select class=\"newAdd\" id=\"new_o\" name=\"new_o\">\n"
							+ "<option value=\"<=\">&le;</option>"
							+ "<option value=\">=\">&ge;</option>"
							+ "<option value=\"=\">=</option>"
							+ "</select>\n";
						result += "</th>\n";
						result += "<th><input type=\"number\" class=\"newAdd\" " +
							"id=\"new_cx0\" " +
							"name=\"new_cx0\" value=\"1\" />\n";
						result += "</th>\n";
						if (nbVars == 2){
							result += "<th>";
							result += "<input type=\"color\" class=\"newAdd\" id=\"new_color\" " +
								"name=\"new_color\" value=\"#ff0000\"/>";
							result += "</th>";
						}

						result += "</tr>\n";
					}
				}

				ViewBag.formula2 = result;

				result = "<h4>AVEC</h4>\n";
				result += "<table  cellspacing=2 cellpadding=2 id=\"data_table_cond\" border=0>\n";
					
				for (int i = 0; i < nbVars; i++) {
					result += "<tr>\n";

					result += "<th>";
					result += "<select id=\"cond"+i+"\" name=\"cond" + i + "\"" +
						" onchange=\"refresh_graphic()\" >\n"
						+ "<option value=\"avec\">Avec</option>"
						+ "<option value=\"sans\">Sans</option>"
						+ "</select>\n";
					result += "x<sub>"+(i+1);
					result += "</th>\n";

					result += "<th>";
					result += "<select id=\"cond"+i+"o\" name=\"cond" + i + "o\"" +
						" onchange=\"refresh_graphic()\" >\n"
						+ "<option value=\">=\">&ge;</option>"
						+ "<option value=\"<=\">&le;</option>"
						+ "</select>\n";
					result += "</th>\n";

					result += "<th>";
					result += 0;
					result += "</th>\n";

					if (nbVars == 2){
						result += "<th>";
						result += "<input type=\"color\" id=\"cond"+i+"color\" " +
							"name=\"cond"+i+"color\" ";

						if (i < listCondtionColors.Length){
							result += "value=\""+listCondtionColors[i]+"\"";
						}else{
							result += "value=\"#ff0000\"";
						}

						result += " onchange=\"refresh_graphic()\" />";
						result += "</th>\n";
					}
					result += "</tr>\n";
				}

				result += "</table>\n";


				ViewBag.Formula3 = result;

				result = "";

				if (nbVars == 2){
					result = "<div id=\"box\" class=\"jxgbox\" style=\"width:800px; " +
						"height:600px;\"></div>\n" +
						"\t<script type=\"text/javascript\">\n ";
					result += "refresh_graphic();\n";
					result += "\t</script>";
				}//if

				ViewBag.graphic = result;
			}catch{
				return Redirect(Url.Action("Index", "Home"));
			}

			return View ();
        }//Index
    }
}
