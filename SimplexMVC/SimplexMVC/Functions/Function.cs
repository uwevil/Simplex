﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SimplexMVC
{
	internal static class Function{
		internal static IEnumerable<int> IndexMax(ClassX[] fFunc){
			var listIndexMax = new List<int>();

			while(listIndexMax.Count != fFunc.Length){
				int indexMax = -1;
				double valueMax = 0;

				for (int i=0; i<fFunc.Length-1; i++){
					if (listIndexMax.Contains(i))
						continue;
					var tmp = (double)fFunc[i].Fraction;
					if (valueMax < tmp){
						valueMax = tmp;
						indexMax = i;
					}
				}
				listIndexMax.Add(indexMax);

				if (indexMax == -1){
					break;
				}
			}

			foreach(int i in listIndexMax)
				yield return i;
		}

		internal static int IndexMin(ClassX[,] fTab, int indexMax){
			int indexMin = -1;
			bool isAssigned = false;

			int size = fTab.GetLength(1);

			var valueMin = new Fraction(-1);
			for (int i=0; i<fTab.GetLength(0); i++){
				Fraction valueTmp;
				if (fTab[i, indexMax].Fraction > 0){
					valueTmp = fTab[i, size-1].Fraction / fTab[i, indexMax].Fraction;

					if (!isAssigned){
						valueMin = valueTmp;
						indexMin = i;
						isAssigned = true;
					}else{
						if (valueMin > valueTmp){
							valueMin = valueTmp;
							indexMin = i;
						}
					}
				}
			}


			return indexMin;
		}

		internal static ClassX[,] CalculTab(ClassX[,] fTab, int indexMin, int indexMax)
		{
			var pivot = fTab[indexMin, indexMax];
			ClassX[,] newFTab = new ClassX[fTab.GetLength(0), fTab.GetLength(1)];

			for (int i=0; i<fTab.GetLength(0); i++){
				for (int j=0; j<fTab.GetLength(1); j++){
					if (i == indexMin && j == indexMax){
						var x = fTab[indexMin, indexMax];
						newFTab[indexMin, indexMax] = new ClassX(x.Name, x.Index, 1);
					}else if (i == indexMin){
						var x = fTab[i,j].Clone();
					//	x.Coeff /= pivot.Coeff;
						x.Fraction = x.Fraction / pivot.Fraction;
						newFTab[i, j] = x;
					}else{
						var x = fTab[i,j].Clone();
						var frac = fTab[i, indexMax].Fraction/pivot.Fraction;
						frac = frac * fTab[indexMin, j].Fraction;
						x.Fraction = x.Fraction - frac;

						newFTab[i,j] = x;
					}
				}
			}

			return newFTab;
		}

		internal static int CalculFunc(out ClassX[] newFFunc, ClassX[,] fTab, ClassX[] fFunc,
		                                int indexMin, int indexMax)
		{
			newFFunc = new ClassX[fFunc.Length];
				
			int isContinue = 1;
			var pivot = fTab[indexMin, indexMax];
			for (int i=0; i<fTab.GetLength(1); i++){
				var x = fFunc[i].Clone();
				var frac = fFunc[indexMax].Fraction / pivot.Fraction;
				frac = frac * fTab[indexMin, i].Fraction;
				x.Fraction = x.Fraction - frac;
				newFFunc[i] = x;

				if (newFFunc[i].Fraction > 0){
					isContinue = 0;
				}
			}
			return isContinue;
		}

		internal static void PersistData(string currentMd5, string goal, int nbVars, int nbCons, 
		                                 ClassConstraint inputTargetFunction, 
		                                 List<ClassConstraint> inputConstraints,
		                                 List<ClassConstraint> inputConditions){
			var dir = Path.GetTempPath();
			var fileName = "download_"+currentMd5+".txt";
			var fullPath = dir + fileName;
			string data = Function.generateData(goal, nbVars, nbCons, inputTargetFunction,
			                                    inputConstraints, inputConditions);
			File.WriteAllText(fullPath, data);
		}

		/*
		* generate data to write to file
		*/
		internal static String generateData(string goal, int nbVars, int nbCons, 
		                                    ClassConstraint inputTargetFunction, 
		                                    List<ClassConstraint> inputConstraints,
		                                    List<ClassConstraint> inputConditions)
		{
			string s = "goal:"+goal+"\n";
			s += "nbVars:"+nbVars+"\n";
			s += "nbCons:"+nbCons+"\n";
			s += "function://x1:x2:...:xn\n";
			for (int i=0; i<inputTargetFunction.LeftMember.Count; i++){
				s += inputTargetFunction.LeftMember[i].Coeff;
				if (i < nbVars-1)
					s += ":";
			}
			s += "\n";
			s += "constraints://x1:x2:...:xn:>= ou >= ou =:b\n";
			foreach(var constraint in inputConstraints){
				for (int i=0; i<constraint.LeftMember.Count; i++){
					var x = constraint.LeftMember[i];
					s += x.Coeff;
					s += ":";
				}
				s += constraint.Operator;
				s += ":";
				s += constraint.RightMember[0].Coeff;
				s += "\n";
			}

			s += "conditions://avec ou sans:<= ou >=:0\n";
			for (int i=0; i<inputConditions.Count; i++){
				var cond = inputConditions[i];
				s += cond.Option;
				s += ":";
				s += cond.Operator;
				s += ":";
				s += "0\n";
			}
			return s;
		}

		internal static byte[] GetFile(string s)
		{
			System.IO.FileStream fs = System.IO.File.OpenRead(s);
			byte[] data = new byte[fs.Length];
			int br = fs.Read(data, 0, data.Length);
			if (br != fs.Length)
				throw new System.IO.IOException(s);
			fs.Close();
			return data;
		}
	}
}
