﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SimplexMVC
{
	public class HashMd5
	{
		private string currentMd5;
		public HashMd5()
		{
			string date = DateTime.Now.Ticks.ToString();
			using (MD5 md5Hash = MD5.Create()){
				currentMd5 = GetMd5Hash(md5Hash, date+"dsqg$jth=ùùm'§'(àç!)àçç!");
			}

		}

		public string get(){
			return currentMd5;
		}

		private string GetMd5Hash(MD5 md5Hash, string input)
		{
			// Convert the input string to a byte array and compute the hash.
			byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

			// Create a new Stringbuilder to collect the bytes
			// and create a string.
			StringBuilder sBuilder = new StringBuilder();

			// Loop through each byte of the hashed data 
			// and format each one as a hexadecimal string.
			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}

			// Return the hexadecimal string.
			return sBuilder.ToString();
		}
	}
}
