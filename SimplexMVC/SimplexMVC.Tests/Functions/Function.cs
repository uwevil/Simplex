﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimplexMVC
{
	internal static class Function
	{
	/*	internal static int IndexMax(ClassX[] fFunc){
			int indexMax = -1;
			double valueMax = 0;

			for (int i=0; i<fFunc.Length-1; i++){
				double tmp = (double)fFunc[i].Fraction;
				if (valueMax < tmp){
					valueMax = tmp;
					indexMax = i;
				}
			}

			return indexMax;
		}*/

		internal static IEnumerable<int> IndexMax(ClassX[] fFunc){
			var listIndexMax = new List<int>();

			while(listIndexMax.Count != fFunc.Length){
				int indexMax = -1;
				double valueMax = 0;

				for (int i=0; i<fFunc.Length-1; i++){
					if (listIndexMax.Contains(i))
						continue;
					var tmp = (double)fFunc[i].Fraction;
					if (valueMax < tmp){
						valueMax = tmp;
						indexMax = i;
					}
				}
				listIndexMax.Add(indexMax);

				if (indexMax == -1){
					break;
				}
			}

			foreach(int i in listIndexMax)
				yield return i;
		}

	/*	internal static int IndexMin(ClassConstraint targetFunction)
		{
			int indexMin = targetFunction.LeftMember[0].Coeff;
			for (int i=1; i<targetFunction.LeftMember.Count; i++){
				int tmp = targetFunction.LeftMember[i].Coeff;
				if (indexMin> tmp){
					indexMin = i;
				}
			}

			return indexMin;
		}
*/
		internal static int IndexMin(ClassX[,] fTab, int indexMax){
			int indexMin = -1;
			bool isAssigned = false;

			int size = fTab.GetLength(1);

			var valueMin = new Fraction(-1);
			for (int i=0; i<fTab.GetLength(0); i++){
				Fraction valueTmp;
				if (fTab[i, indexMax].Fraction > 0){
					valueTmp = fTab[i, size-1].Fraction / fTab[i, indexMax].Fraction;

					if (!isAssigned){
						valueMin = valueTmp;
						indexMin = i;
						isAssigned = true;
					}else{
						if (valueMin > valueTmp){
							valueMin = valueTmp;
							indexMin = i;
						}
					}
				}
			}


			return indexMin;
		}

		internal static ClassX[,] CalculTab(ClassX[,] fTab, int indexMin, int indexMax)
		{
			var pivot = fTab[indexMin, indexMax];
			ClassX[,] newFTab = new ClassX[fTab.GetLength(0), fTab.GetLength(1)];

			for (int i=0; i<fTab.GetLength(0); i++){
				for (int j=0; j<fTab.GetLength(1); j++){
					if (i == indexMin && j == indexMax){
						var x = fTab[indexMin, indexMax];
						newFTab[indexMin, indexMax] = new ClassX(x.Name, x.Index, 1);
					}else if (i == indexMin){
						var x = fTab[i,j].Clone();
					//	x.Coeff /= pivot.Coeff;
						x.Fraction = x.Fraction / pivot.Fraction;
						newFTab[i, j] = x;
					}else{
						var x = fTab[i,j].Clone();
						var frac = fTab[i, indexMax].Fraction/pivot.Fraction;
						frac = frac * fTab[indexMin, j].Fraction;
						x.Fraction = x.Fraction - frac;

						newFTab[i,j] = x;
					}
				}
			}

			return newFTab;
		}

		internal static bool CalculFunc(out ClassX[] newFFunc, ClassX[,] fTab, ClassX[] fFunc,
		                                int indexMin, int indexMax)
		{
			newFFunc = new ClassX[fFunc.Length];
				
			bool isContinue = false;
			var pivot = fTab[indexMin, indexMax];
			for (int i=0; i<fTab.GetLength(1); i++){
				var x = fFunc[i].Clone();
				var frac = fFunc[indexMax].Fraction / pivot.Fraction;
				frac = frac * fTab[indexMin, i].Fraction;
				x.Fraction = x.Fraction - frac;
				newFFunc[i] = x;

				if (newFFunc[i].Fraction > 0){
					isContinue = true;
				}
			}
			return isContinue;
		}
	}
}
