﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace SimplexMVC.Tests{
	[TestFixture]
	public class FunctionTest
	{
		[Test]
		public void MinMax()
		{
			// Arrange
			var fFunc = new ClassX[5];
			for (int i=0; i< fFunc.Length; i++){
				fFunc[i] = new ClassX("x", i+1, 1);
			}

			fFunc[0].Coeff = -10;
			fFunc[1].Coeff = -2;
			fFunc[2].Coeff = -5;
			fFunc[3].Coeff = 0;
			fFunc[4].Coeff = -2;

			// Act
			var result = new List<int>();
			var resultTest = new List<int> {0,2,1};

			foreach (int i in Function.IndexMax(fFunc)){
				result.Add(i);
				Console.Out.Write(i);
			}

		//	Console.Out.Write(int);

			// Assert
		//	Assert.AreEqual(result, resultTest);
		//	Assert.AreEqual(expectedRuntime, result.ViewData["Runtime"]);
		}
	}
}